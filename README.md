# Spark Streaming

## Introduction
This project read the data from csv file that is generated by data-ingestion.  Reads each line of csv line by line and does a micro batch processing.

For inrix use case, the data is received every minute.
CSV file contains around 50000 records but only around 3000(interstate roads) are filtered and processed as per Pranamesh's inrix incident detection algorithm.
InrixStreams class has good Java Docs which explains the algorithm used.
application.properties is where parameters can be specified. 

## Pre-requisites
1. Java 8: https://www.digitalocean.com/community/tutorials/how-to-install-java-with-apt-get-on-debian-8
2. Spark:  wget http://www-us.apache.org/dist/spark/spark-2.3.3/spark-2.3.3-bin-hadoop2.7.tgz�
   tar xvzf spark-2.3.2-bin-hadoop2.7.tgz
   SPARK_HOME=/DeZyre/spark 
   export PATH=$SPARK_HOME/bin:$PATH
   source  ~/.bashrc
   Run ./bin/spark-shell
3. Redis
   nstallation: https://redis.io/topics/quickstart
   wget http://download.redis.io/redis-stable.tar.gz
   sudo apt install redis-server
   nohup redis-server &

## Services used:
For Mongo connection, maven dependencies are added - mongo-spark-connector_2.10.
Internal spark streaming APIs are used to generate streams, for pre processing, map functions are used.


## To build the jar :
mvn clean install

## To run in local :

1. Install spark in local.
2. In your IDE, go to ReadingStreams class and Run as main class. (configure the paths in Constants).

## Steps to deploy in Microsoft Azure :
First Spark needs to be setup.

1. SSH : ssh team@23.101.126.159 
2. Password : Clust3rax$$$
3. Copy jar to Spark cluster (from local machine):  
   scp target/spark-streaming-1.0-SNAPSHOT.jar ssh team@23.101.126.159:spark-streaming.jar

3. To run jar in spark cluster: 
   nohup /usr/hdp/current/spark2-client/bin/spark-submit spark-streaming.jar &
 
## Machine to deploy in AWS: 
ssh -i "timeli.pem" ubuntu@ec2-3-91-13-27.compute-1.amazonaws.com
 
For more details on logic, Please look at Raviteja's Creative Component Doc.


## Know Issues

1. Some cases duplicate incidents are detected (with same event_id and duplicate = false)
2. It stops retrieving data from csv file after some days and stops spark.



