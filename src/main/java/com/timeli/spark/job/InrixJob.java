//package com.timeli.spark.job;
//
//import com.mongodb.MongoClient;
//import com.mongodb.MongoClientURI;
//import com.mongodb.client.MongoCollection;
//import com.mongodb.client.MongoDatabase;
//import com.timeli.spark.obj.Incident;
//import com.timeli.spark.streams.InrixData;
//import com.timeli.spark.utils.Utils;
//import org.apache.spark.api.java.JavaPairRDD;
//import org.apache.spark.api.java.JavaRDD;
//import org.apache.spark.api.java.function.FlatMapFunction;
//import org.apache.spark.api.java.function.Function;
//import org.apache.spark.api.java.function.PairFunction;
//import org.apache.spark.api.java.function.VoidFunction;
//import org.bson.Document;
//import redis.clients.jedis.Jedis;
//import redis.clients.jedis.JedisPool;
//import redis.clients.jedis.JedisPoolConfig;
//import scala.Tuple2;
//
//import java.io.IOException;
//import java.io.Serializable;
//import java.text.DateFormat;
//import java.text.SimpleDateFormat;
//import java.time.LocalDateTime;
//import java.time.format.DateTimeFormatter;
//import java.util.*;
//import java.util.function.Consumer;
//
//public class InrixJob implements Serializable {
//
//    public InrixJob() {
//
//    }
//
//    private static HashMap<String, String> codeToEventId = new HashMap<>();
//    private static HashMap<String, Incident> codeToIncident = new HashMap<>();
//    //    private static Jedis jedis;
////    private static JedisPool pool = new JedisPool(new JedisPoolConfig(), "localhost");
//    private static MongoCollection<Document> inrixDataCollection;
//    private static int id;
//    private static HashMap<String, List<String>> timestampToCode = new HashMap<>();
//    private static HashMap<String, List<String>> codeToTimestamp = new HashMap<>();
//
//    public static void initialize() {
//        //Opening the mongodb connection
//        initializeMongodb();
//    }
//
//    private static void initializeMongodb() {
//        MongoClient mongo = new MongoClient(new MongoClientURI(mongoConnectionString));
//        MongoDatabase mongodb = mongo.getDatabase(mongoDbName);
//        inrixDataCollection = mongodb.getCollection("inrixbatchdata");
//    }
//
//    public static void executeInrixJob(JavaRDD<String> stringJavaRDD) {
//        JedisPoolHolder.init("localhost", 6379);
////        JedisPool pool = new JedisPool(new JedisPoolConfig(), "localhost");
//
////        jedis = new Jedis("localhost", 6379, 10000);
////        JavaRDD<String> stringJavaRDD = jsc.textFile("/Users/ravitejarajabyna/ra/timeli-sparkstreaming/src/main/resources/inrix.csv");
//        System.out.println("Number of lines in file = " + stringJavaRDD.count());
//        System.out.println("Start Time: " + new Date(System.currentTimeMillis()));
//
//        JavaRDD<InrixData> inrixData = stringJavaRDD.map(convertToInrixData());
//        System.out.println(inrixData.count());
//
//        JavaRDD<InrixData> targetRoutesInrixData = inrixData.filter(filterTargetRoutes());
//        System.out.println(targetRoutesInrixData.count());
////        inrixData.repartition(10).filter(filterTargetRoutes());
//
//        JavaRDD<InrixData> filteredOutput = targetRoutesInrixData.filter(filterByCvalueAndScoreAndMiles());
//        System.out.println(filteredOutput.count());
//        JavaPairRDD<String, Iterable<InrixData>> groupByCode = filteredOutput.groupBy(new Function<InrixData, String>() {
//            @Override
//            public String call(InrixData v1) throws Exception {
//                return v1.getCode();
//            }
//        });
//        System.out.println(groupByCode.count());
//        JavaPairRDD<String, List<String>> incident = groupByCode.mapToPair(incidents());
//
//
//
////        Tuple2<String, List<String>> first = incident.first();
//        Map<String, List<String>> stringListMap = incident.collectAsMap();
//
//        System.out.println(stringListMap);
//        System.out.println("End Time: " + new Date(System.currentTimeMillis()));
//
////        groupByCode.foreach(groupBy());
//
////        JavaPairRDD<Incident, InrixData> mapToPair = filteredOutput.mapToPair(mapToIncidents2());
//
//    }
//
//    private static PairFunction<Tuple2<String, Iterable<InrixData>>, String, List<String>> incidents() {
//        return new PairFunction<Tuple2<String, Iterable<InrixData>>, String, List<String>>() {
//            @Override
//            public Tuple2<String, List<String>> call(Tuple2<String, Iterable<InrixData>> stringIterableTuple2) throws Exception {
//
//                stringIterableTuple2._2.forEach(new Consumer<InrixData>() {
//                    @Override
//                    public void accept(InrixData inrixDataObj) {
//                        Jedis jedis = null;
//                        try {
//                            jedis = JedisPoolHolder.getInstance().getResource();
//
//                            final double speed = Double.parseDouble(inrixDataObj.getSpeed());
//                            final String code = inrixDataObj.getCode();
//                            final String startTimestamp = inrixDataObj.getTimestamp();
//                            final double thresholdSpeed = getThresholdSpeed(startTimestamp, code, jedis);
//                            final int score = Integer.parseInt(inrixDataObj.getScore());
//                            final int cvalue = Integer.parseInt(inrixDataObj.getCvalue());
//                            final Incident potentialIncident = getPotentialIncident(code, speed, thresholdSpeed, startTimestamp, score, cvalue, jedis);
//                            if (potentialIncident.getIncident().equals("INCIDENT")) {
//                                String startTS = potentialIncident.getStartTS();
//                                String c = potentialIncident.getCode();
//                                if (codeToTimestamp.containsKey(c)) {
////                                timestampToCode.put(potentialIncident.getStartTS(), potentialIncident.getCode());
//                                    List<String> timestamps = codeToTimestamp.get(c);
//                                    timestamps.add(startTS);
//                                    codeToTimestamp.put(c, timestamps);
//                                } else {
//                                    codeToTimestamp.put(c, new ArrayList<>());
//                                }
//                            }
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        } finally {
//                            if (jedis != null) {
//                                jedis.close();
//                            }
//                        }
//                    }
//                });
//
//                return new Tuple2<>(stringIterableTuple2._1, codeToTimestamp.get(stringIterableTuple2._1));
//            }
//        };
//    }
//
////    private static VoidFunction<Tuple2<String, Iterable<InrixData>>> groupBy() {
////        return new VoidFunction<Tuple2<String, Iterable<InrixData>>>() {
////            @Override
////            public void call(Tuple2<String, Iterable<InrixData>> stringIterableTuple2) throws Exception {
////                stringIterableTuple2._2.forEach(new Consumer<InrixData>() {
////                    @Override
////                    public void accept(InrixData inrixDataObj) {
////                        final double speed = Double.parseDouble(inrixDataObj.getSpeed());
////                        final String code = inrixDataObj.getCode();
////                        final String startTimestamp = inrixDataObj.getTimestamp();
////                        final double thresholdSpeed = getThresholdSpeed(startTimestamp, code);
////                        final int score = Integer.parseInt(inrixDataObj.getScore());
////                        final int cvalue = Integer.parseInt(inrixDataObj.getCvalue());
////                        final Incident potentialIncident = getPotentialIncident(code, speed, thresholdSpeed, startTimestamp, score, cvalue);
////                        if (potentialIncident.getIncident().equals("INCIDENT")) {
////                            timestampToCode.put(potentialIncident.getStartTS(), potentialIncident.getCode());
////                            codeToTimestamp.put(potentialIncident.getCode(), potentialIncident.getStartTS());
////                        }
////                    }
////                });
////            }
////        };
////    }
//
//    private static Function<String, InrixData> convertToInrixData() {
//        return new Function<String, InrixData>() {
//            @Override
//            public InrixData call(String s) throws Exception {
//                return csvToInrixData(s);
//            }
//        };
//    }
//
//    private static InrixData csvToInrixData(String csvRecord) throws IOException {
//        String splitBy = ",";
//        String[] inrixDataArray = csvRecord.split(splitBy);
//        if (inrixDataArray.length == 9) {
//            return new InrixData(inrixDataArray[0], inrixDataArray[1], inrixDataArray[2],
//                    inrixDataArray[3], inrixDataArray[4], inrixDataArray[5], inrixDataArray[6],
//                    inrixDataArray[7], inrixDataArray[8]);
//        }
//        return null;
//
//    }
//
//    private static Function<InrixData, Boolean> filterTargetRoutes() {
//        return new Function<InrixData, Boolean>() {
//            @Override
//            public Boolean call(InrixData inrixDataObj) throws Exception {
//                Jedis jedis = null;
//                try {
//                    jedis = JedisPoolHolder.getInstance().getResource();
//                    if (inrixDataObj != null) {
//                        String code = inrixDataObj.getCode();
//                        String targetCode = jedis.hget(code, "code");
//                        if (targetCode != null) {
//                            return true;
//                        }
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                } finally {
//                    if (jedis != null) {
//                        jedis.close();
//                    }
//                }
//
//                return false;
//            }
//        };
//    }
//
//    private static Function<InrixData, Boolean> filterByCvalueAndScoreAndMiles() {
//        return new Function<InrixData, Boolean>() {
//            @Override
//            public Boolean call(InrixData inrixDataObj) throws Exception {
//                LocalDateTime datetime = getDateTime(inrixDataObj.getTimestamp());
//                String code = inrixDataObj.getCode();
//                Jedis jedis = null;
//                double miles = 0.0;
//                try {
//                    jedis = JedisPoolHolder.getInstance().getResource();
//                    String startlat = jedis.hget(code, "startlat");
//                    String distance = jedis.hget(code, "distance");
//
//                    if (inrixDataObj.getScore().length() < 1 ||
//                            inrixDataObj.getCvalue().length() < 1 || inrixDataObj.getSpeed().isEmpty()
//                            || datetime == null || distance == null || startlat == null) {
//                        return false;
//                    }
////                int score = Integer.parseInt(inrixDataObj.getScore());
////                int cvalue = Integer.parseInt(inrixDataObj.getCvalue());
////                double speed = Double.parseDouble(inrixDataObj.getSpeed());
//                    miles = Double.parseDouble(distance);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                } finally {
//                    if (jedis != null) {
//                        jedis.close();
//                    }
//                }
//                return miles > 0.2;
//            }
//        };
//    }
//
//    private static LocalDateTime getDateTime(String startTimestamp) {
//        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss z");
//        LocalDateTime datetime = null;
//        try {
//            datetime = LocalDateTime.parse(startTimestamp, formatter);
//        } catch (Exception e) {
//            System.out.println("cannot parse date");
//        }
//        return datetime;
//    }
//
//    private static double getThresholdSpeed(String startTimestamp, String code, Jedis jedis) {
//        double thresholdSpeed;
//        LocalDateTime datetime = getDateTime(startTimestamp);
//        //in java Monday = 1 and Sunday = 7. Do mod 7 to match with the
//        //datafile inrix_param_may31.csv where sunday=0 and saturday=6
//        int weekday = 0;
//        int hour = 0;
//        int min = 0;
//        if (startTimestamp != null) {
//            weekday = datetime.getDayOfWeek().getValue() % 7;
//            hour = datetime.getHour();
//            min = datetime.getMinute();
//        }
//        double period = getPeriod(min);
//        String key = Utils.genKey(code, weekday, hour);
////        Jedis jedis = pool.getResource();
//        String tspeed = jedis.hget(key, String.valueOf(period));
//        if (tspeed != null)
//            thresholdSpeed = Double.parseDouble(tspeed);
//        else
//            thresholdSpeed = 0.0;    //This is an workaround. Need to correct later
//        return thresholdSpeed;
//    }
//
//    private static double getPeriod(int min) {
//        double period = 0.0;
//
//        if ((min >= 0) && (min <= 14))
//            period = 0.0;
//        else if ((min >= 15) && (min <= 29))
//            period = 1.0;
//        else if ((min >= 30) && (min <= 44))
//            period = 2.0;
//        else if ((min >= 45) && (min <= 59))
//            period = 3.0;
//
//        return period;
//    }
//
//
//    private static Incident getPotentialIncident(String code, double speed, double thresholdSpeed, String startTimestamp,
//                                                 int score, int cvalue, Jedis jedis) {
//        Incident incident = codeToIncident.get(code);
//        if (speed < thresholdSpeed && speed < 45.0 && (score == 30) && (cvalue >= 30)) {
//            incident = updateIncidentCount(incident, code, startTimestamp, jedis);
//        } else if (codeToIncident.containsKey(code) && !incident.getIncident().equals("IGNORE")) {
//            incident = updateIncidentOverCount(incident, code, jedis);
//        } else {
//            incident = new Incident(startTimestamp, "IGNORE",
//                    0, 0, 0, code, "Not an Event", null);
//        }
//        codeToIncident.put(code, incident);
//        return incident;
//    }
//
//    private static Incident updateIncidentCount(Incident incident, String code, String startTimestamp, Jedis jedis) {
//        if (codeToIncident.containsKey(code)) {
//            int incidentCount = incident.getIncidentCount();
//            String incidentState = incident.getIncident();
//            incidentCount++;
//            if (incidentCount < 3) {
//                incident.setIncidentCount(incidentCount);
//                incident.setIncident("POTENTIAL");
//                incident.setOverCount(0);
//                incident.setCurrentIncident(0);
//                incident.setEventId("Not an event");
//                System.out.println("POTENTIAL : " + incident);
//            } else if (incidentCount == 3 && incidentState.equals("POTENTIAL")) {
//                incident.setIncidentCount(incidentCount);
//                incident.setOverCount(0);
//                incident.setCurrentIncident(1);
//                incident.setIncident("INCIDENT");
//                incident.setEventId(generateEventId(incident.getIncident(), code, jedis));
//                System.out.println("INCIDENT : " + incident);
//            } else {
//                incident.setIncidentCount(incidentCount);
//                incident.setOverCount(0);
//                incident.setCurrentIncident(1);
//                incident.setIncident("IN_PROGRESS");
//                System.out.println("IN PROGRESS : " + incident);
//            }
//        } else {
//            incident = new Incident(startTimestamp, "POTENTIAL",
//                    0, 1, 0, code, "Not an Event", null);
//            System.out.println("POTENTIAL : " + incident);
//        }
//        return incident;
//    }
//
//    private static Incident updateIncidentOverCount(Incident incident, String code, Jedis jedis) {
//        int incidentCount = incident.getIncidentCount();
//        String incidentState = incident.getIncident();
//        int overCount = incident.getOverCount();
//        if (incidentCount < 3) {
//            incident.setCurrentIncident(0);
//            incident.setIncident("IGNORE");
//            incident.setOverCount(0);
//            incident.setIncidentCount(0);
//            incident.setEventId("Not an Event");
//            incident.setEndTimestamp(null);
//            System.out.println("IGNORE after POTENTIAL : " + incident);
//        } else if (overCount < 5) {
//            incident.setOverCount(overCount + 1);
//            incident.setIncident("MAY_BE_OVER");
//            incident.setCurrentIncident(1);
//            incident.setIncidentCount(incidentCount);
//            System.out.println("MAY Be OVER : " + incident);
//        } else {
//            incident.setOverCount(overCount);
//            incident.setIncident("OVER");
//            incident.setCurrentIncident(0);
//            incident.setIncidentCount(0);
//            incident.setEventId(generateEventId(incidentState, code, jedis));
//            incident.setEndTimestamp(new Date(System.currentTimeMillis()).toString());
//            System.out.println("OVER : " + incident);
//        }
//        return incident;
//    }
//
//    private static String generateEventId(String incident, String code, Jedis jedis) {
//        String eventId = "Not an event";
//        if (incident.equals("OVER")) {
//            eventId = codeToEventId.get(code);
//            codeToEventId.remove(code);
//        } else if (incident.equals("INCIDENT")) {
//            String key = code + "L";
//            long count = jedis.llen(key);
//            List<String> codes = jedis.lrange(key, 0, count - 1);
//            eventId = null;
//            for (String segmentcode : codes) {
//                if (codeToEventId.get(segmentcode) != null) {
//                    eventId = codeToEventId.get(segmentcode);
//                    codeToEventId.put(code, eventId);
//                    break;
//                }
//            }
//            if (eventId == null) {
//                id++;
//                Date today = new Date();
//                DateFormat df2 = new SimpleDateFormat("dd-MM-yyyy");
//                String dateString = df2.format(today);
//                eventId = dateString + "-" + id;
//                codeToEventId.put(code, eventId);
//            }
//        }
//        return eventId;
//    }
//}
