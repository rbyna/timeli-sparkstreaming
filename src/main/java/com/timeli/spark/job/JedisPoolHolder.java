package com.timeli.spark.job;


import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

public class JedisPoolHolder {
    /** {@link JedisPool} */
    private static volatile JedisPool instance = null;

    /** redis ip */
    private static String ip;

    /** redis port */
    private static int port;

    /**
     * 初始化
     * <p>
     *
     * @param redisIp
     * @param redisPort
     */
    public static void init(String redisIp, int redisPort) {
        ip = redisIp;
        port = redisPort;
    }

    /**
     * 获取单例JedisPool
     * <p>
     *
     * @return {@link JedisPool}
     */
    public static JedisPool getInstance() {
        if (instance == null) {
            synchronized (JedisPoolHolder.class) {
                if (instance == null) {
                    JedisPoolConfig config = new JedisPoolConfig();
                    config.setMaxTotal(100);
                    config.setMinIdle(10);
                    config.setMaxIdle(10);
                    config.setMaxWaitMillis(2000);
                    config.setTestWhileIdle(false);
                    config.setTestOnBorrow(false);
                    config.setTestOnReturn(false);
                    instance = new JedisPool(config, ip, port);
                }
            }
        }
        return instance;
    }
}
