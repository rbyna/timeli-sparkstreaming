package com.timeli.spark.lib;


import com.timeli.spark.obj.RawInrixData;
import com.timeli.spark.utils.DateUtils;
import org.bson.BsonDateTime;
import org.bson.types.BSONTimestamp;
import org.json.JSONArray;
import org.json.JSONObject;

import com.timeli.spark.obj.LocationCodeFreeway;
import com.timeli.spark.obj.SegmentLocationData;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;

import static java.util.stream.Collectors.toList;

public class CsvToJsonConvertor {



    public static String csvToJson() throws IOException {
        JSONArray objects = new JSONArray(Files.readAllLines(Paths.get("src/main/resources/location_codes_freeway.csv"))
                .stream()
                .map(s -> new LocationCodeFreeway(s.split(",")[0], s.split(",")[1], s.split(",")[2], s.split(",")[3],
                        s.split(",")[4], s.split(",")[5], s.split(",")[6], s.split(",")[7]))
                .collect(toList()));
        return objects.toString();

    }

    public static String segmentLocationDataCsvToJson(String s) throws IOException {
//        String[] locationDataArray = s.split(",");
        String[] locationDataArray = s.split(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)", -1);
        Date start_timestamp = DateUtils.convertTime(locationDataArray[2]);
        JSONObject jsonObject = null;
        try {
            SegmentLocationData locationData = new SegmentLocationData(locationDataArray[0], locationDataArray[1], locationDataArray[2],
                    locationDataArray[3], locationDataArray[4], Double.parseDouble(locationDataArray[5]), Double.parseDouble(locationDataArray[6]), locationDataArray[7],
                    Boolean.parseBoolean(locationDataArray[8]), Integer.parseInt(locationDataArray[9]), Integer.parseInt(locationDataArray[10]), Integer.parseInt(locationDataArray[11]),
                    Integer.parseInt(locationDataArray[12]), locationDataArray[13], locationDataArray[14], Integer.parseInt(locationDataArray[15]),
                    locationDataArray[16], locationDataArray[17], locationDataArray[18], locationDataArray[19],
                    locationDataArray[20], locationDataArray[21], locationDataArray[22], Double.parseDouble(locationDataArray[23]),
                    locationDataArray[24], locationDataArray[25], locationDataArray[26], Double.parseDouble(locationDataArray[27]),
                    Double.parseDouble(locationDataArray[28]), Double.parseDouble(locationDataArray[29]), Double.parseDouble(locationDataArray[30]), locationDataArray[31], locationDataArray[32], locationDataArray[33], new Date(System.currentTimeMillis()), start_timestamp, null, Boolean.valueOf(locationDataArray[34]));

             jsonObject = new JSONObject(locationData);
        } catch (NumberFormatException e) {
            System.out.println("Unable to parse location data " + s);

        }
        //System.out.println(locationDataArray[0]);
        //System.out.println(jsonObject.toString());
        return jsonObject != null ? jsonObject.toString() : null;

    }



    public static String rawDataCsvToJson(String s) throws IOException {
//        String[] locationDataArray = s.split(",");
        String[] locationDataArray = s.split(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)", -1);
        JSONObject jsonObject = null;
        try {
            RawInrixData locationData = new RawInrixData(locationDataArray[0],
                    Double.parseDouble(locationDataArray[1]), Double.parseDouble(locationDataArray[2]), Double.parseDouble(locationDataArray[3]), locationDataArray[4], Integer.parseInt(locationDataArray[5]),
                    Integer.parseInt(locationDataArray[6]), locationDataArray[7], locationDataArray[8], Integer.parseInt(locationDataArray[9]),
                    locationDataArray[10], locationDataArray[11], locationDataArray[12], locationDataArray[13],
                    locationDataArray[14], locationDataArray[15], locationDataArray[16], Double.parseDouble(locationDataArray[17]),
                    locationDataArray[18], locationDataArray[19], locationDataArray[20], Double.parseDouble(locationDataArray[21]),
                    Double.parseDouble(locationDataArray[22]), Double.parseDouble(locationDataArray[23]), Double.parseDouble(locationDataArray[24]), locationDataArray[25], locationDataArray[26], locationDataArray[27], new BsonDateTime(System.currentTimeMillis()));

            jsonObject = new JSONObject(locationData);
            //System.out.println(locationDataArray[0]);
            //System.out.println(jsonObject.toString());
        } catch (NumberFormatException e) {
            System.out.println("Unable to parse raw data " + s);
        }
        return jsonObject != null ? jsonObject.toString() : null;

    }
    private static String getISODateString(Date date) {
        long time = date.getTime();return String.valueOf(time);
    }

    public static void main(String[] args) throws IOException {
//        String json = segmentLocationDataCsvToJson("1485615236,66,67,30,1485615236,92,XDS,0.522,71,21991,10147850," +
//                "1485615236,1485615253,1485615221,0,35,I 35, ,United States of America,Iowa,Decatur, ,0.6166798395004,0, " +
//                ",I 35,40.83186,-93.81306,40.83938,-93.80670,N,3082440,4326");
//        System.out.println(json);

        String s = rawDataCsvToJson("1450489749,0.0,67,66,Inrix,1874,9813472,1450489735,1450489761,0,35,I 35;IA 27, ,United States of America,Iowa,Worth, ,0.58874920442,0, ,I 35;IA 27,43.37931,-93.34952,43.37079,-93.34940,S,2624584,4326,aflaksdjfklasjfklasdjfkl");
        System.out.println(s);
    }


}
