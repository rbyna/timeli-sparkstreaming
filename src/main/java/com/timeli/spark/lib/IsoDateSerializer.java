package com.timeli.spark.lib;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializable;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.jsontype.TypeSerializer;

import java.io.IOException;
import java.util.Date;

@JsonSerialize
public class IsoDateSerializer extends JsonSerializer<Date> {

    @Override
    public void serialize(Date value, JsonGenerator gen, SerializerProvider serializers) throws IOException, JsonProcessingException {
        String dateValue = getISODateString(value);
        String text = "{ \"$date\" : \""+   dateValue   +"\"}";
        gen.writeRawValue(text);
    }

    private String getISODateString(Date date) {
        long time = date.getTime();return String.valueOf(time);
    }

}
