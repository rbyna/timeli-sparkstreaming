package com.timeli.spark.lib;

import com.mongodb.client.FindIterable;
import com.timeli.spark.utils.DateUtils;
import javafx.util.Pair;
import org.apache.commons.collections.MultiMap;
import org.bson.Document;

import com.mongodb.BasicDBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

import javax.print.Doc;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.concurrent.TimeUnit;

import static com.mongodb.client.model.Filters.*;

public class MongoUpdate {

	public static boolean update(MongoCollection<Document> collection,
			String code, String endOfIncidentTimeStamp, String incident) {
		BasicDBObject newdoc = new BasicDBObject();
		if(incident.equals("OVER")) {
			FindIterable<Document> incidentDetails = collection.find(and(eq("code", code), eq("currentIncident", 1) ));
//			System.out.println(incidentDetails);//incident_inprogress
			if (incidentDetails.iterator().hasNext()) {
				Document doc = incidentDetails.iterator().next();
//				System.out.println(doc.toString());
				Boolean incident_inprogress = doc.getBoolean("incident_inprogress");
//				System.out.println("Progress : " + incident_inprogress);
				boolean inProgress = incident_inprogress == null ? false : incident_inprogress;
				if(!inProgress) {
//				if (incident_inprogress == null && !incident_inprogress) {
					newdoc.append("$set", new BasicDBObject().append("end_timestamp", endOfIncidentTimeStamp)
							.append("incident_inprogress", false)
							.append("currentIncident", 0));//.append("end_timestamp_date_format", DateUtils.convertTime(endOfIncidentTimeStamp)));
					BasicDBObject searchquery = new BasicDBObject().append("code", code).append("currentIncident", 1);
					newdoc.append("$currentDate",
							new BasicDBObject("end_timestamp_date_format", true));
					collection.findOneAndUpdate(searchquery, newdoc);
					return true;
				} else {
					//Set autocomplete = true, informing operator that this segment is no longer an incident
					System.out.println("Incident with code " + code + " is in progress. Cannot set it to OVER");
					newdoc.append("$set", new BasicDBObject()
							.append("autocomplete", true));//.append("end_timestamp_date_format", DateUtils.convertTime(endOfIncidentTimeStamp)));
					BasicDBObject searchquery = new BasicDBObject().append("code", code).append("currentIncident", 1);
					newdoc.append("$currentDate",
							new BasicDBObject("end_timestamp_date_format", true));
					collection.findOneAndUpdate(searchquery, newdoc);
					return false;
				}
			} else {
				System.out.println("Incident with code : " + code + " is not found in db");
			}

			
		}
		return false;

	}

	public static FindIterable<Document> getOldIncidents(MongoCollection<Document> collection, Date startTime) {
		return collection.find(and(eq("currentIncident", 0), gte("end_timestamp_date_format", startTime.getTime())));

	}

	public static FindIterable<Document> getCurrentIncidents(MongoCollection<Document> collection) {
		return collection.find(eq("currentIncident", 1));

	}

	public static void saveIncident(MongoCollection<Document> collection, Document document) {
		collection.insertOne(document);
	}

	public static void updateStartTimeStamp(MongoCollection<Document> collection, Document doc) {
		String code = doc.getString("code");
		BasicDBObject update = new BasicDBObject("$currentDate",
				new BasicDBObject("start_timestamp_date_format", true)
		);

			BasicDBObject searchquery = new BasicDBObject().append("code", code).append("currentIncident", 1);
			collection.findOneAndUpdate(searchquery, update);
	}

	public static void updateDuplicate(MongoCollection<Document> collection, Document doc) {
		String code = doc.getString("code");
		String event_id = doc.getString("event_id");
        BasicDBObject newdoc = new BasicDBObject();
        newdoc.append("$set", new BasicDBObject().append("duplicate", true));
		BasicDBObject searchquery = new BasicDBObject().append("code", code).append("currentIncident", 1).append("event_id", event_id);
		collection.findOneAndUpdate(searchquery, newdoc);
	}

//	public static void main(String[] args) {
//		HashMap<String, String> codeToTimeStamp = new HashMap<>();
//		String mongoConnectionString = "mongodb://timelicosmosdb:Mj8JiJqbwZALW4dUkT9VdLY8PUdWhEgUwuog8uuksrZTTHVTBaDzjwKphQMy59y78nf7gvW20b8guKMrtpwuRw==@timelicosmosdb.documents.azure.com:10255/?ssl=true&replicaSet=globaldb";
//		//public final static String mongoConnectionString = "mongodb://127.0.0.1:27017/timelidb";
//  		String mongoDbName = "timelidb";
//		String mongoCollectionName = "inrixdata";
//		MongoClient mongo = new MongoClient(new MongoClientURI(mongoConnectionString));
//		MongoDatabase mongodb = mongo.getDatabase(mongoDbName);
//		MongoCollection<Document> inrixDataCollection = mongodb.getCollection(mongoCollectionName);
//
////		Date enddate = Calendar.getInstance().getTime();
//
//		Date startDate = new Date(System.currentTimeMillis() - TimeUnit.HOURS.toMillis(1));
//
//		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss z");
//		LocalDateTime startDateTime = getDateTime(df.format(startDate));
//		for (Document doc : getOldIncidents(inrixDataCollection)) {
//			String timestamp = String.valueOf(doc.get("start_timestamp"));
//			System.out.println(timestamp);
//			LocalDateTime dateTime = getDateTime(timestamp);
//			if (dateTime.isAfter(startDateTime)){
//				String code = String.valueOf(doc.get("code"));
//				codeToTimeStamp.put(code, dateTime.toString());
//			}
//
//		}
//		System.out.println(codeToTimeStamp);
//	}
	private static LocalDateTime getDateTime(String startTimestamp) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss z");
		LocalDateTime datetime = null;
		try {
			datetime = LocalDateTime.parse(startTimestamp, formatter);
		} catch (Exception e) {
			System.out.println("cannot parse date");
		}

		return datetime;
	}

	private static FindIterable<Document> getOIs(MongoCollection<Document> inrixDataCollection,
										Date startDate, Date endDate) {
		BasicDBObject query = new BasicDBObject("startTimestampDate",
				new BasicDBObject("$lte",startDate));
		FindIterable<Document> documents = inrixDataCollection.find(query);
		return documents;
	}

	public static void main(String[] args) {

		String mongoConnectionString = "mongodb://timelidb:NXQf7S3iOdWdRS9kswt8YrGOJhjUKVti1lYK9kQ24KSZMIkYI8zYfIeRCyL76icFNuqRr87UTjFhKfQ6aZ61HA==@timelidb.documents.azure.com:10255/?ssl=true&replicaSet=globaldb";
		//public final static String mongoConnectionString = "mongodb://127.0.0.1:27017/timelidb";
  		String mongoDbName = "timelidb";
		String mongoCollectionName = "inrixdata";
		MongoClient mongo = new MongoClient(new MongoClientURI(mongoConnectionString));
		MongoDatabase mongodb = mongo.getDatabase(mongoDbName);
		MongoCollection<Document> inrixDataCollection = mongodb.getCollection(mongoCollectionName);
//		boolean update = MongoUpdate.update(inrixDataCollection, "1485644080", null, "OVER");
		FindIterable<Document> oIs = getOIs(inrixDataCollection, getOnHourEarlyDate(), getCurrentDate());
		System.out.println(oIs.iterator().hasNext());
		System.out.println(getOnHourEarlyDate());
		for(Document doc: oIs) {
			System.out.println(doc);
		}
//		System.out.println();
	}

	private static Date getCurrentDate() {
		final Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
		return cal.getTime();
//		return  Calendar.getInstance().getTime(TimeZone.getTimeZone("GMT"));
	}



	private static Date getOnHourEarlyDate() {
		final Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
		cal.add(Calendar.DATE, -1);
		return cal.getTime();
	}

}
