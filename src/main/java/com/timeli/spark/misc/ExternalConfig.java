package com.timeli.spark.misc;

public class ExternalConfig {

    private String mongoConnectionString;
    private String mongoDbName;
    private String mongoCollectionNameForProcessedInrixData;
    private String mongoCollectionNameForRawInrixData;
    private String inrixDirPath;

    public String getInrixFilename() {
        return inrixFilename;
    }

    public void setInrixFilename(String inrixFilename) {
        this.inrixFilename = inrixFilename;
    }

    private String inrixFilename;
    private String incidentsOccurredInPastInMinutes;
//    private String

    public ExternalConfig() {
    }

    public ExternalConfig(String mongoConnectionString, String mongoDbName,
                          String mongoCollectionNameForProcessedInrixData,
                          String mongoCollectionNameForRawInrixData,
                          String inrixDirPath, String inrixFilename, String incidentsOccurredInPastInMinutes) {
        this.mongoConnectionString = mongoConnectionString;
        this.mongoDbName = mongoDbName;
        this.mongoCollectionNameForProcessedInrixData = mongoCollectionNameForProcessedInrixData;
        this.mongoCollectionNameForRawInrixData = mongoCollectionNameForRawInrixData;
        this.inrixDirPath = inrixDirPath;
        this.incidentsOccurredInPastInMinutes = incidentsOccurredInPastInMinutes;
        this.inrixFilename = inrixFilename;
    }


    public String getMongoConnectionString() {
        return mongoConnectionString;
    }

    public void setMongoConnectionString(String mongoConnectionString) {
        this.mongoConnectionString = mongoConnectionString;
    }

    public String getMongoDbName() {
        return mongoDbName;
    }

    public void setMongoDbName(String mongoDbName) {
        this.mongoDbName = mongoDbName;
    }

    public String getMongoCollectionNameForProcessedInrixData() {
        return mongoCollectionNameForProcessedInrixData;
    }

    public void setMongoCollectionNameForProcessedInrixData(String mongoCollectionNameForProcessedInrixData) {
        this.mongoCollectionNameForProcessedInrixData = mongoCollectionNameForProcessedInrixData;
    }

    public String getMongoCollectionNameForRawInrixData() {
        return mongoCollectionNameForRawInrixData;
    }

    public void setMongoCollectionNameForRawInrixData(String mongoCollectionNameForRawInrixData) {
        this.mongoCollectionNameForRawInrixData = mongoCollectionNameForRawInrixData;
    }

    public String getInrixDirPath() {
        return inrixDirPath;
    }

    public void setInrixDirPath(String inrixDirPath) {
        this.inrixDirPath = inrixDirPath;
    }



    public String getIncidentsOccurredInPastInMinutes() {
        return incidentsOccurredInPastInMinutes;
    }

    public void setIncidentsOccurredInPastInMinutes(String incidentsOccurredInPastInMinutes) {
        this.incidentsOccurredInPastInMinutes = incidentsOccurredInPastInMinutes;
    }

    @Override
    public String toString() {
        return "ExternalConfig{" +
                "mongoConnectionString='" + mongoConnectionString + '\'' +
                ", mongoDbName='" + mongoDbName + '\'' +
                ", mongoCollectionNameForProcessedInrixData='" + mongoCollectionNameForProcessedInrixData + '\'' +
                ", mongoCollectionNameForRawInrixData='" + mongoCollectionNameForRawInrixData + '\'' +
                ", inrixDirPath='" + inrixDirPath + '\'' +
                ", inrixFilename='" + inrixFilename + '\'' +
                ", incidentsOccurredInPastInMinutes='" + incidentsOccurredInPastInMinutes + '\'' +
                '}';
    }
}
