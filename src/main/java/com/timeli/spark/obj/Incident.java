package com.timeli.spark.obj;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import redis.clients.jedis.GeoRadiusResponse;
import redis.clients.jedis.Jedis;

public class Incident {

    public void setStartTS(String startTS) {
        this.startTS = startTS;
    }

    private String startTS;
    private String incident;
    private int currentIncident;
    private int incidentCount;

    public String getEndTimestamp() {
        return endTimestamp;
    }

    public void setEndTimestamp(String endTimestamp) {
        this.endTimestamp = endTimestamp;
    }

    private String endTimestamp;

    public int getOverCount() {
        return overCount;
    }

    public void setOverCount(int overCount) {
        this.overCount = overCount;
    }

    private int overCount;
    private String code;
    private String eventId;

    public boolean isDuplicate() {
        return duplicate;
    }

    public void setDuplicate(boolean duplicate) {
        this.duplicate = duplicate;
    }

    private boolean duplicate;


    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public Incident(String startTS, String incident, int currentIncident, int incidentCount, int overCount, String code, String eventId,
                    String endTimestamp, boolean duplicate) {
        this.startTS = startTS;
    	this.incident = incident;
        this.currentIncident = currentIncident;
        this.incidentCount = incidentCount;
        this.overCount = overCount;
        this.code = code;
        this.eventId = eventId;
        this.endTimestamp = endTimestamp;
        this.duplicate = duplicate;
    }
    
    public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
	public String getEventId() {
		return eventId;
	}

	public String getStartTS() {
    	return startTS;
    }

    public String getIncident() {
        return incident;
    }

    public void setIncident(String incident) {
        this.incident = incident;
    }

    public int getCurrentIncident() {
        return currentIncident;
    }

    public void setCurrentIncident(int currentIncident) {
        this.currentIncident = currentIncident;
    }

    public int getIncidentCount() {
        return incidentCount;
    }

    public void setIncidentCount(int incidentCount) {
        this.incidentCount = incidentCount;
    }


    @Override
    public String toString() {
        return "Incident{" +
                "startTS='" + startTS + '\'' +
                ", incident='" + incident + '\'' +
                ", currentIncident=" + currentIncident +
                ", incidentCount=" + incidentCount +
                ", endTimestamp='" + endTimestamp + '\'' +
                ", overCount=" + overCount +
                ", code='" + code + '\'' +
                ", eventId='" + eventId + '\'' +
                ", duplicate=" + duplicate +
                '}';
    }
}
