package com.timeli.spark.obj;

import java.io.Serializable;

public class LocationCodeFreeway implements Serializable{


    private String code;

    private String precode;

    private String Start_lat;

    private String Start_long;

    private String End_Lat;

    private String End_Long;

    private String Distance;

    private String Road;

    public LocationCodeFreeway(String code, String precode, String start_lat, String start_long, String end_Lat, String end_Long, String distance, String road) {
        this.code = code;
        this.precode = precode;
        Start_lat = start_lat;
        Start_long = start_long;
        End_Lat = end_Lat;
        End_Long = end_Long;
        Distance = distance;
        Road = road;
    }

    public LocationCodeFreeway() {

    }


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getPrecode() {
        return precode;
    }

    public void setPrecode(String precode) {
        this.precode = precode;
    }

    public String getStart_lat() {
        return Start_lat;
    }

    public void setStart_lat(String start_lat) {
        Start_lat = start_lat;
    }

    public String getStart_long() {
        return Start_long;
    }

    public void setStart_long(String start_long) {
        Start_long = start_long;
    }

    public String getEnd_Lat() {
        return End_Lat;
    }

    public void setEnd_Lat(String end_Lat) {
        End_Lat = end_Lat;
    }

    public String getEnd_Long() {
        return End_Long;
    }

    public void setEnd_Long(String end_Long) {
        End_Long = end_Long;
    }

    public String getDistance() {
        return Distance;
    }

    public void setDistance(String distance) {
        Distance = distance;
    }

    public String getRoad() {
        return Road;
    }

    public void setRoad(String road) {
        Road = road;
    }
}
