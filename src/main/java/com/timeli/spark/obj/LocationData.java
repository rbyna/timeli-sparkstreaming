package com.timeli.spark.obj;

import java.io.Serializable;

public class LocationData implements Serializable {

    private String code;
    private String previous_code;
    private String next_code;
    private String frc;
    private String fid;
    private String oid_1;
    private String linearid;
    private String roadname;
    private String roadnumber;
    private String country;
    private String state;
    private String district;
    private String county;
    private String miles;
    private String sliproad;
    private String specialroad;
    private String roadlist;
    private String startlat;
    private String startlong;
    private String endlat;
    private String endlong;
    private String bearing;
    private String xdgroup;
    private String shapesrid;


    public LocationData(String code, String previous_code, String next_code, String frc, String fid, String oid_1, String linearid, String roadname, String roadnumber, String country, String state, String district, String county, String miles, String sliproad, String specialroad, String roadlist, String startlat, String startlong, String endlat, String endlong, String bearing, String xdgroup, String shapesrid) {
        this.code = code;
        this.previous_code = previous_code;
        this.next_code = next_code;
        this.frc = frc;
        this.fid = fid;
        this.oid_1 = oid_1;
        this.linearid = linearid;
        this.roadname = roadname;
        this.roadnumber = roadnumber;
        this.country = country;
        this.state = state;
        this.district = district;
        this.county = county;
        this.miles = miles;
        this.sliproad = sliproad;
        this.specialroad = specialroad;
        this.roadlist = roadlist;
        this.startlat = startlat;
        this.startlong = startlong;
        this.endlat = endlat;
        this.endlong = endlong;
        this.bearing = bearing;
        this.xdgroup = xdgroup;
        this.shapesrid = shapesrid;
    }

    public LocationData() {
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getPrevious_code() {
        return previous_code;
    }

    public void setPrevious_code(String previous_code) {
        this.previous_code = previous_code;
    }

    public String getNext_code() {
        return next_code;
    }

    public void setNext_code(String next_code) {
        this.next_code = next_code;
    }

    public String getFrc() {
        return frc;
    }

    public void setFrc(String frc) {
        this.frc = frc;
    }

    public String getFid() {
        return fid;
    }

    public void setFid(String fid) {
        this.fid = fid;
    }

    public String getOid_1() {
        return oid_1;
    }

    public void setOid_1(String oid_1) {
        this.oid_1 = oid_1;
    }

    public String getLinearid() {
        return linearid;
    }

    public void setLinearid(String linearid) {
        this.linearid = linearid;
    }

    public String getRoadname() {
        return roadname;
    }

    public void setRoadname(String roadname) {
        this.roadname = roadname;
    }

    public String getRoadnumber() {
        return roadnumber;
    }

    public void setRoadnumber(String roadnumber) {
        this.roadnumber = roadnumber;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getMiles() {
        return miles;
    }

    public void setMiles(String miles) {
        this.miles = miles;
    }

    public String getSliproad() {
        return sliproad;
    }

    public void setSliproad(String sliproad) {
        this.sliproad = sliproad;
    }

    public String getSpecialroad() {
        return specialroad;
    }

    public void setSpecialroad(String specialroad) {
        this.specialroad = specialroad;
    }

    public String getRoadlist() {
        return roadlist;
    }

    public void setRoadlist(String roadlist) {
        this.roadlist = roadlist;
    }

    public String getStartlat() {
        return startlat;
    }

    public void setStartlat(String startlat) {
        this.startlat = startlat;
    }

    public String getStartlong() {
        return startlong;
    }

    public void setStartlong(String startlong) {
        this.startlong = startlong;
    }

    public String getEndlat() {
        return endlat;
    }

    public void setEndlat(String endlat) {
        this.endlat = endlat;
    }

    public String getEndlong() {
        return endlong;
    }

    public void setEndlong(String endlong) {
        this.endlong = endlong;
    }

    public String getBearing() {
        return bearing;
    }

    public void setBearing(String bearing) {
        this.bearing = bearing;
    }

    public String getXdgroup() {
        return xdgroup;
    }

    public void setXdgroup(String xdgroup) {
        this.xdgroup = xdgroup;
    }

    public String getShapesrid() {
        return shapesrid;
    }

    public void setShapesrid(String shapesrid) {
        this.shapesrid = shapesrid;
    }
}
