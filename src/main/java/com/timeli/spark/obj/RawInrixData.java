package com.timeli.spark.obj;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.timeli.spark.lib.IsoDateSerializer;
import org.bson.BSON;
import org.bson.BSONObject;
import org.bson.BsonDateTime;
import org.bson.types.BSONTimestamp;

import java.io.Serializable;
import java.util.Date;

public class RawInrixData implements Serializable {

    private String code;
    private double threshold_speed;
    private double speed;
    private double avg_speed;
    private String author;
    private int fid;
    private int oid_1;
    private String previous_code;
    private String next_code;
    private int frc;
    private String roadnumber;
    private String roadname;
    private String linearid;
    private String country;
    private String state;
    private String county;
    private String district;
    private double miles;
    private String slipRoad;
    private String specialroad;
    private String roadlist;
    private double start_lat;
    private double start_long;
    private double end_lat;
    private double end_long;
    private String bearing;
    private String xdGroup;
    private String shapesrid;

    public BsonDateTime getTime() {
        return time;
    }

    public void setTime(BsonDateTime time) {
        this.time = time;
    }

    private BsonDateTime time;

    public RawInrixData() {
    }

    public RawInrixData(String code, double threshold_speed, double speed, double avg_speed, String author,
                        int fid, int oid_1, String previous_code, String next_code, int frc, String roadnumber,
                        String roadname, String linearid, String country, String state, String county,
                        String district, double miles, String slipRoad, String specialroad, String roadlist,
                        double start_lat, double start_long, double end_lat, double end_long, String bearing,
                        String xdGroup, String shapesrid, BsonDateTime time) {
        this.code = code;
        this.threshold_speed = threshold_speed;
        this.speed = speed;
        this.avg_speed = avg_speed;
        this.author = author;
        this.fid = fid;
        this.oid_1 = oid_1;
        this.previous_code = previous_code;
        this.next_code = next_code;
        this.frc = frc;
        this.roadnumber = roadnumber;
        this.roadname = roadname;
        this.linearid = linearid;
        this.country = country;
        this.state = state;
        this.county = county;
        this.district = district;
        this.miles = miles;
        this.slipRoad = slipRoad;
        this.specialroad = specialroad;
        this.roadlist = roadlist;
        this.start_lat = start_lat;
        this.start_long = start_long;
        this.end_lat = end_lat;
        this.end_long = end_long;
        this.bearing = bearing;
        this.xdGroup = xdGroup;
        this.shapesrid = shapesrid;
        this.time = time;
    }


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public double getThreshold_speed() {
        return threshold_speed;
    }

    public void setThreshold_speed(double threshold_speed) {
        this.threshold_speed = threshold_speed;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public double getAvg_speed() {
        return avg_speed;
    }

    public void setAvg_speed(double avg_speed) {
        this.avg_speed = avg_speed;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public int getFid() {
        return fid;
    }

    public void setFid(int fid) {
        this.fid = fid;
    }

    public int getOid_1() {
        return oid_1;
    }

    public void setOid_1(int oid_1) {
        this.oid_1 = oid_1;
    }

    public String getPrevious_code() {
        return previous_code;
    }

    public void setPrevious_code(String previous_code) {
        this.previous_code = previous_code;
    }

    public String getNext_code() {
        return next_code;
    }

    public void setNext_code(String next_code) {
        this.next_code = next_code;
    }

    public int getFrc() {
        return frc;
    }

    public void setFrc(int frc) {
        this.frc = frc;
    }

    public String getRoadnumber() {
        return roadnumber;
    }

    public void setRoadnumber(String roadnumber) {
        this.roadnumber = roadnumber;
    }

    public String getRoadname() {
        return roadname;
    }

    public void setRoadname(String roadname) {
        this.roadname = roadname;
    }

    public String getLinearid() {
        return linearid;
    }

    public void setLinearid(String linearid) {
        this.linearid = linearid;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public double getMiles() {
        return miles;
    }

    public void setMiles(double miles) {
        this.miles = miles;
    }

    public String getSlipRoad() {
        return slipRoad;
    }

    public void setSlipRoad(String slipRoad) {
        this.slipRoad = slipRoad;
    }

    public String getSpecialroad() {
        return specialroad;
    }

    public void setSpecialroad(String specialroad) {
        this.specialroad = specialroad;
    }

    public String getRoadlist() {
        return roadlist;
    }

    public void setRoadlist(String roadlist) {
        this.roadlist = roadlist;
    }

    public double getStart_lat() {
        return start_lat;
    }

    public void setStart_lat(double start_lat) {
        this.start_lat = start_lat;
    }

    public double getStart_long() {
        return start_long;
    }

    public void setStart_long(double start_long) {
        this.start_long = start_long;
    }

    public double getEnd_lat() {
        return end_lat;
    }

    public void setEnd_lat(double end_lat) {
        this.end_lat = end_lat;
    }

    public double getEnd_long() {
        return end_long;
    }

    public void setEnd_long(double end_long) {
        this.end_long = end_long;
    }

    public String getBearing() {
        return bearing;
    }

    public void setBearing(String bearing) {
        this.bearing = bearing;
    }

    public String getXdGroup() {
        return xdGroup;
    }

    public void setXdGroup(String xdGroup) {
        this.xdGroup = xdGroup;
    }

    public String getShapesrid() {
        return shapesrid;
    }

    public void setShapesrid(String shapesrid) {
        this.shapesrid = shapesrid;
    }
}
