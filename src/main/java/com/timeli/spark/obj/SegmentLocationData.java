package com.timeli.spark.obj;

import java.io.Serializable;
import java.util.Date;

public class SegmentLocationData implements Serializable {
	public SegmentLocationData() {

	}

	private String event_id;
	private String code;
	private String start_timestamp;
	private String end_timestamp;
	private String threshold_speed;
	private double speed;
	private double avg_speed;
	private String author;
	private boolean incident_inprogress;
	private int incident_count;
	private int currentIncident;
	private int fid;
	private int oid_1;
	private String previous_code;
	private String next_code;
	private int frc;
	private String roadnumber;
	private String roadname;
	private String linearid;
	private String country;
	private String state;
	private String county;
	private String district;
	private double miles;
	private String slipRoad;
	private String specialroad;
	private String roadlist;
	private double start_lat;
	private double start_long;
	private double end_lat;
	private double end_long;
	private String bearing;
	private String xdGroup;
	private String shapesrid;

	public Date getStartTimestampDate() {
		return startTimestampDate;
	}

	public void setStartTimestampDate(Date startTimestampDate) {
		this.startTimestampDate = startTimestampDate;
	}

	private Date startTimestampDate;

//	@DateTimeFormat(iso= DateTimeFormat.ISO.DATE_TIME)
	private Date start_timestamp_date_format;

	public Date getStart_timestamp_date_format() {
		return start_timestamp_date_format;
	}

	public void setStart_timestamp_date_format(Date start_timestamp_date_format) {
		this.start_timestamp_date_format = start_timestamp_date_format;
	}

	public Date getEnd_timestamp_date_format() {
		return end_timestamp_date_format;
	}

	public void setEnd_timestamp_date_format(Date end_timestamp_date_format) {
		this.end_timestamp_date_format = end_timestamp_date_format;
	}

	//	@DateTimeFormat(iso= DateTimeFormat.ISO.DATE_TIME)
	private Date end_timestamp_date_format;

	public boolean isDuplicate() {
		return duplicate;
	}

	public void setDuplicate(boolean duplicate) {
		this.duplicate = duplicate;
	}

	private boolean duplicate;

	public SegmentLocationData(String event_id, String code, String start_timestamp, String end_timestamp,
							   String threshold_speed, double speed, double avg_speed, String author,
							   boolean incident_inprogress, int incident_count, int currentIncident,
							   int fid, int oid_1, String previous_code, String next_code, int frc,
							   String roadnumber, String roadname, String linearid, String country,
							   String state, String county, String district, double miles, String slipRoad,
							   String specialroad, String roadlist, double start_lat, double start_long,
							   double end_lat, double end_long, String bearing, String xdGroup, String shapesrid,
							   Date startTimestampDate, Date start_timestamp_date_format, Date end_timestamp_date_format, boolean duplicate) {
		this.event_id = event_id;
		this.code = code;
		this.start_timestamp = start_timestamp;
		this.end_timestamp = end_timestamp;
		this.threshold_speed = threshold_speed;
		this.speed = speed;
		this.avg_speed = avg_speed;
		this.author = author;
		this.incident_inprogress = incident_inprogress;
		this.incident_count = incident_count;
		this.currentIncident = currentIncident;
		this.fid = fid;
		this.oid_1 = oid_1;
		this.previous_code = previous_code;
		this.next_code = next_code;
		this.frc = frc;
		this.roadnumber = roadnumber;
		this.roadname = roadname;
		this.linearid = linearid;
		this.country = country;
		this.state = state;
		this.county = county;
		this.district = district;
		this.miles = miles;
		this.slipRoad = slipRoad;
		this.specialroad = specialroad;
		this.roadlist = roadlist;
		this.start_lat = start_lat;
		this.start_long = start_long;
		this.end_lat = end_lat;
		this.end_long = end_long;
		this.bearing = bearing;
		this.xdGroup = xdGroup;
		this.shapesrid = shapesrid;
		this.startTimestampDate = startTimestampDate;
		this.start_timestamp_date_format = start_timestamp_date_format;
		this.end_timestamp_date_format = end_timestamp_date_format;
	}


	public String getEvent_id() {
		return event_id;
	}

	public void setEvent_id(String event_id) {
		this.event_id = event_id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getStart_timestamp() {
		return start_timestamp;
	}

	public void setStart_timestamp(String start_timestamp) {
		this.start_timestamp = start_timestamp;
	}

	public String getEnd_timestamp() {
		return end_timestamp;
	}

	public void setEnd_timestamp(String end_timestamp) {
		this.end_timestamp = end_timestamp;
	}

	public String getThreshold_speed() {
		return threshold_speed;
	}

	public void setThreshold_speed(String threshold_speed) {
		this.threshold_speed = threshold_speed;
	}

	public double getSpeed() {
		return speed;
	}

	public void setSpeed(double speed) {
		this.speed = speed;
	}

	public double getAvg_speed() {
		return avg_speed;
	}

	public void setAvg_speed(double avg_speed) {
		this.avg_speed = avg_speed;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public boolean isIncident_inprogress() {
		return incident_inprogress;
	}

	public void setIncident_inprogress(boolean incident_inprogress) {
		this.incident_inprogress = incident_inprogress;
	}

	public int getIncident_count() {
		return incident_count;
	}

	public void setIncident_count(int incident_count) {
		this.incident_count = incident_count;
	}

	public int getCurrentIncident() {
		return currentIncident;
	}

	public void setCurrentIncident(int currentIncident) {
		this.currentIncident = currentIncident;
	}

	public int getFid() {
		return fid;
	}

	public void setFid(int fid) {
		this.fid = fid;
	}

	public int getOid_1() {
		return oid_1;
	}

	public void setOid_1(int oid_1) {
		this.oid_1 = oid_1;
	}

	public String getPrevious_code() {
		return previous_code;
	}

	public void setPrevious_code(String previous_code) {
		this.previous_code = previous_code;
	}

	public String getNext_code() {
		return next_code;
	}

	public void setNext_code(String next_code) {
		this.next_code = next_code;
	}

	public int getFrc() {
		return frc;
	}

	public void setFrc(int frc) {
		this.frc = frc;
	}

	public String getRoadnumber() {
		return roadnumber;
	}

	public void setRoadnumber(String roadnumber) {
		this.roadnumber = roadnumber;
	}

	public String getRoadname() {
		return roadname;
	}

	public void setRoadname(String roadname) {
		this.roadname = roadname;
	}

	public String getLinearid() {
		return linearid;
	}

	public void setLinearid(String linearid) {
		this.linearid = linearid;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public double getMiles() {
		return miles;
	}

	public void setMiles(double miles) {
		this.miles = miles;
	}

	public String getSlipRoad() {
		return slipRoad;
	}

	public void setSlipRoad(String slipRoad) {
		this.slipRoad = slipRoad;
	}

	public String getSpecialroad() {
		return specialroad;
	}

	public void setSpecialroad(String specialroad) {
		this.specialroad = specialroad;
	}

	public String getRoadlist() {
		return roadlist;
	}

	public void setRoadlist(String roadlist) {
		this.roadlist = roadlist;
	}

	public double getStart_lat() {
		return start_lat;
	}

	public void setStart_lat(double start_lat) {
		this.start_lat = start_lat;
	}

	public double getStart_long() {
		return start_long;
	}

	public void setStart_long(double start_long) {
		this.start_long = start_long;
	}

	public double getEnd_lat() {
		return end_lat;
	}

	public void setEnd_lat(double end_lat) {
		this.end_lat = end_lat;
	}

	public double getEnd_long() {
		return end_long;
	}

	public void setEnd_long(double end_long) {
		this.end_long = end_long;
	}

	public String getBearing() {
		return bearing;
	}

	public void setBearing(String bearing) {
		this.bearing = bearing;
	}

	public String getXdGroup() {
		return xdGroup;
	}

	public void setXdGroup(String xdGroup) {
		this.xdGroup = xdGroup;
	}

	public String getShapesrid() {
		return shapesrid;
	}

	public void setShapesrid(String shapesrid) {
		this.shapesrid = shapesrid;
	}
}
