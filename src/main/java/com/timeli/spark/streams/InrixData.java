package com.timeli.spark.streams;

import java.io.Serializable;

public class InrixData implements Serializable{
    private String code;
    private String cvalue;
    private String reference;
    private String score;
    private String speed;
    private String average;
    private String type;
    private String travelTimeMinutes;
    private String timestamp;


    //For serialization
    public InrixData() {
    }

    public InrixData(String code, String cvalue, String reference, String score, String speed, String average, String type, String travelTimeMinutes, String timestamp) {
        this.code = code;
        this.cvalue = cvalue;
        this.reference = reference;
        this.score = score;
        this.speed = speed;
        this.average = average;
        this.type = type;
        this.travelTimeMinutes = travelTimeMinutes;
        this.timestamp = timestamp;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCvalue() {
        return cvalue;
    }

    public void setCvalue(String cvalue) {
        this.cvalue = cvalue;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public String getSpeed() {
        return speed;
    }

    public void setSpeed(String speed) {
        this.speed = speed;
    }

    public String getAverage() {
        return average;
    }

    public void setAverage(String average) {
        this.average = average;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTravelTimeMinutes() {
        return travelTimeMinutes;
    }

    public void setTravelTimeMinutes(String travelTimeMinutes) {
        this.travelTimeMinutes = travelTimeMinutes;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        return "InrixData{" +
                "code='" + code + '\'' +
                ", cvalue='" + cvalue + '\'' +
                ", reference='" + reference + '\'' +
                ", score='" + score + '\'' +
                ", speed='" + speed + '\'' +
                ", average='" + average + '\'' +
                ", type='" + type + '\'' +
                ", travelTimeMinutes='" + travelTimeMinutes + '\'' +
                ", timestamp='" + timestamp + '\'' +
                '}';
    }
}
