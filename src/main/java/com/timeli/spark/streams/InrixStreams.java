package com.timeli.spark.streams;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.MongoClientURI;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.spark.MongoSpark;
import com.mongodb.spark.config.WriteConcernConfig;
import com.mongodb.spark.config.WriteConfig;
import com.timeli.spark.job.JedisPoolHolder;
import com.timeli.spark.misc.ExternalConfig;
import com.timeli.spark.obj.Incident;
import com.timeli.spark.obj.LocationData;
import com.timeli.spark.lib.CsvToJsonConvertor;
import com.timeli.spark.utils.DateUtils;
import com.timeli.spark.utils.JedisUtils;
import com.timeli.spark.lib.MongoUpdate;
import com.timeli.spark.utils.Utils;
import javafx.util.Pair;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFunction;
import org.apache.spark.api.java.function.VoidFunction;
import org.apache.spark.streaming.api.java.JavaDStream;
import org.apache.spark.streaming.api.java.JavaPairDStream;
import org.bson.Document;
import redis.clients.jedis.Jedis;
import scala.Tuple2;

import java.io.IOException;
import java.io.Serializable;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static com.timeli.spark.misc.Constants.*;

public class InrixStreams implements Serializable {

    public InrixStreams() {

    }

    private static HashMap<String, String> codeToEventId = new HashMap<>();
    private static HashMap<String, Incident> codeToIncident = new HashMap<>();
    private static Jedis jedis;
    private static MongoCollection<Document> inrixDataCollection;
    private static int id;
    private static List<String> codes = new ArrayList<>();
    private static List<String> codesAndItsNeighbours = new ArrayList<>();


    public static void initialize(ExternalConfig config) {
        //Opening the mongodb connection
        initializeMongodb(config);
        //When spark reloads, need to load the pre existing incidents from MongoDB and save into map
        loadCurrentIncidents(inrixDataCollection);
        //Load all the incidents that have occured one hour before current time

    }



    public static void main(String[] args) {
        Date date3 = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss z");
//        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss z");
        System.out.println(df.format(date3));
    }

    private static void initializeMongodb(ExternalConfig config) {
        MongoClientOptions.Builder options_builder = new MongoClientOptions.Builder();
        options_builder.maxConnectionIdleTime(600);
        options_builder.socketKeepAlive(true);
        MongoClientOptions options = options_builder.build();
        MongoClient mongo = new MongoClient(new MongoClientURI(config.getMongoConnectionString()));
        MongoDatabase mongodb = mongo.getDatabase(config.getMongoDbName());
        inrixDataCollection = mongodb.getCollection(config.getMongoCollectionNameForProcessedInrixData());
    }


    private static void loadCurrentIncidents(MongoCollection<Document> inrixDataCollection) {
        FindIterable<Document> currentIncidents = MongoUpdate.getCurrentIncidents(inrixDataCollection);
        for (Document doc : currentIncidents) {
            String code = String.valueOf(doc.get("code"));
            String startTimestamp = String.valueOf(doc.get("start_timestamp"));
            String eventId = String.valueOf(doc.get("event_id"));
            Object secondary = doc.get("duplicate");
            boolean duplicate = false;
            if (secondary != null) {
                duplicate = Boolean.valueOf(String.valueOf(secondary));
            }
            codeToEventId.put(code, eventId);
            codeToIncident.put(code, new Incident(startTimestamp, "IN_PROGRESS", 1, 4, 0, code, eventId, null, duplicate));
        }
        System.out.println("Previous Incident loaded : " + codeToIncident);
    }

    public static void processInrixStreams(JavaDStream<String> inrixData, ExternalConfig config) {
        //inrixData.count().print();
        JedisPoolHolder.init("23.101.126.159", 6379);
        jedis = JedisPoolHolder.getInstance().getResource();
        jedis.auth("Clust3rax$");
        //convert the input inrix stream to object
        JavaDStream<InrixData> inrixDataObjStream = inrixData.map(convertToInrixData());
        //filter the interstate segments
        JavaDStream<InrixData> filteredIncidents = inrixDataObjStream.filter(filterTargetRoutes());
//        System.out.println("Number of segments after filtering target routes : " + filteredIncidents.count());
/*        JavaDStream<InrixData> processedData = filteredIncidents.filter(cleanData());

        FindIterable<Document> currentIncidents = MongoUpdate.getCurrentIncidents(inrixDataCollection);
        codes.retainAll(Collections.EMPTY_LIST);
        for (Document doc : currentIncidents) {
            String code = String.valueOf(doc.get("code"));
            codes.add(code);
            String key = code + "L";
            long count = jedis.llen(key);
            List<String> nearbyCodes = jedis.lrange(key, 0, count - 1);
            codes.addAll(nearbyCodes);
        }

        JavaDStream<InrixData> filteredSegments = processedData.filter(filterOnlyIncidentSegments());
        //Pushing raw data to mongodb
        JavaDStream<Document> rawData = filteredSegments.map(convertRawDataToMongoDocument());
        saveIncidentsToMongo(rawData, new WriteConfig(config.getMongoDbName(), config.getMongoCollectionNameForRawInrixData(),
                scala.Option.apply(config.getMongoConnectionString()),
                false, 500, 0, WriteConcernConfig.Default()));*/
//        rawData.dstream().saveAsTextFiles("/Users/ravitejarajabyna/ra/datasource/data", ".csv");

        //Consider segments with cvalue>=30, score=30 length of the segment > 0.2
        JavaDStream<InrixData> filteredIncidents2 = filteredIncidents.filter(filterByCvalueAndScoreAndMiles());
//        System.out.println("Number of segments after filtering miles > 0.2 : " + filteredIncidents2.count());
        //Find the potential incidents
        JavaPairDStream<Incident, InrixData> incidentAndDataMap = filteredIncidents2.mapToPair(mapToIncidents2(Long.valueOf(config.getIncidentsOccurredInPastInMinutes())));
        //filter the potential incidents with incident tag as "INCIDENT" and update the ones' with incident OVER
        JavaPairDStream<Incident, InrixData> potentialIncidents = incidentAndDataMap.filter(filterPotentialIncidents());
//        System.out.println(potentialIncidents.count());
        //Convert incidents to mongo document
        JavaPairDStream<Incident, Document> mapIncidents = potentialIncidents.mapToPair(convertToMongoDoc());
//        JavaDStream<Document> documents = potentialIncidents.map(convertToMongoDocument2());
        //Save the incidents
//        saveIncidentsToMongo(documents, new WriteConfig(config.getMongoDbName(), config.getMongoCollectionNameForProcessedInrixData(),
//                scala.Option.apply(config.getMongoConnectionString()),
//                false, 500, 0, WriteConcernConfig.Default()));
        saveIncidentsToMongo2(mapIncidents, new WriteConfig(config.getMongoDbName(), config.getMongoCollectionNameForProcessedInrixData(),
                scala.Option.apply(config.getMongoConnectionString()),
                false, 500, 0, WriteConcernConfig.Default()));
    }

    private static Function<InrixData, Boolean> cleanData() {
        return new Function<InrixData, Boolean>() {
            @Override
            public Boolean call(InrixData inrixDataObj) throws Exception {
                LocalDateTime datetime = getDateTime(inrixDataObj.getTimestamp());
                String code = inrixDataObj.getCode();
                String startlat = jedis.hget(code, "startlat");
                String distance = jedis.hget(code, "distance");

                if (inrixDataObj.getScore().length() < 1 ||
                        inrixDataObj.getCvalue().length() < 1 || inrixDataObj.getSpeed().isEmpty()
                        || datetime == null || distance == null || startlat == null) {
//                    System.out.println("Fail to parse the data "+ inrixDataObj);
                    return false;
                }
                return true;
            }
        };
    }

    private static Function<InrixData, Document> convertRawDataToMongoDocument() {
        return new Function<InrixData, Document>() {
            @Override
            public Document call(InrixData inrixData) throws Exception {
                String code = inrixData.getCode();
                String startTimestamp = inrixData.getTimestamp();
                double thresholdSpeed = getThresholdSpeed(startTimestamp, code, jedis);
//                Date start_timestamp = DateUtils.convertTime(locationDataArray[2]);
                LocationData locationData = JedisUtils.retrieveLocationDataFromJedis(jedis, code);
                final String finalInrixString = code + "," + thresholdSpeed
                        + "," + inrixData.getSpeed() + "," + inrixData.getAverage() + "," + "Inrix" + ","
                        + locationData.getFid() + "," + locationData.getOid_1() + "," + locationData.getPrevious_code() + "," + locationData.getNext_code() + "," + locationData.getFrc() + "," + locationData.getRoadnumber() + "," + locationData.getRoadname() + ","
                        + locationData.getLinearid() + "," + locationData.getCountry() + "," + locationData.getState() + "," + locationData.getCounty() + "," + locationData.getDistrict() + "," + locationData.getMiles() + "," + locationData.getSliproad() + "," +
                        locationData.getSpecialroad() + "," + locationData.getRoadlist() + "," + locationData.getStartlat() + "," + locationData.getStartlong() + "," + locationData.getEndlat() + "," + locationData.getEndlong() + "," +
                        locationData.getBearing() + "," + locationData.getXdgroup() + "," + locationData.getShapesrid() + "," + startTimestamp;
                String segmentLocationDataCsvToJson = CsvToJsonConvertor.rawDataCsvToJson(finalInrixString);
                Document doc = Document.parse(segmentLocationDataCsvToJson);
                return doc;
            }
        };
    }

    private static Function<String, InrixData> convertToInrixData() {
        return new Function<String, InrixData>() {
            @Override
            public InrixData call(String s) throws Exception {
                return csvToInrixData(s);
            }
        };
    }

    private static Function<InrixData, Boolean> filterOnlyIncidentSegments() {
        return new Function<InrixData, Boolean>() {
            @Override
            public Boolean call(InrixData inrixDataObj) throws Exception {
                if (inrixDataObj != null) {
                    String code = inrixDataObj.getCode();
//                    String targetCode = jedis.hget(code, "code");
                    if (codes.contains(code)) {
                        return true;
                    }
                }
                return false;
            }
        };
    }

    private static Function<InrixData, Boolean> filterTargetRoutes() {
        return new Function<InrixData, Boolean>() {
            @Override
            public Boolean call(InrixData inrixDataObj) throws Exception {
                if (inrixDataObj != null) {
                    String code = inrixDataObj.getCode();
                    String targetCode = jedis.hget(code, "code");
                    if (targetCode != null) {
                        return true;
                    }
                }
                return false;
            }
        };
    }

    private static Function<InrixData, Boolean> filterByCvalueAndScoreAndMiles() {
        return new Function<InrixData, Boolean>() {
            @Override
            public Boolean call(InrixData inrixDataObj) throws Exception {
                LocalDateTime datetime = getDateTime(inrixDataObj.getTimestamp());
                String code = inrixDataObj.getCode();
                String startlat = jedis.hget(code, "startlat");
                String distance = jedis.hget(code, "distance");

                if (inrixDataObj.getScore().length() < 1 ||
                        inrixDataObj.getCvalue().length() < 1 || inrixDataObj.getSpeed().isEmpty()
                        || datetime == null || distance == null || startlat == null) {
//                    System.out.println("Fail to parse the data " + inrixDataObj);
                    return false;
                }
                double miles = 0.0;
                try {
                    int score = Integer.parseInt(inrixDataObj.getScore());
                    int cvalue = Integer.parseInt(inrixDataObj.getCvalue());
                    double speed = Double.parseDouble(inrixDataObj.getSpeed());
                     miles = Double.parseDouble(distance);
                } catch (NumberFormatException e) {
//                    System.out.println("Fail to parse the miles for data " + inrixDataObj);
                    return false;
                }
                return miles > 0.2;
            }
        };
    }

    public static PairFunction<InrixData, Incident, InrixData> mapToIncidents2(long incidentsOccurredInPastInMinutes) {
        return new PairFunction<InrixData, Incident, InrixData>() {
            @Override
            public Tuple2<Incident, InrixData> call(InrixData inrixDataObj) throws Exception {
                double speed = Double.parseDouble(inrixDataObj.getSpeed());
                String code = inrixDataObj.getCode();
                String startTimestamp = inrixDataObj.getTimestamp();
                double thresholdSpeed = getThresholdSpeed(startTimestamp, code, jedis);
                int score = Integer.parseInt(inrixDataObj.getScore());
                int cvalue = Integer.parseInt(inrixDataObj.getCvalue());
                Incident potentialIncident = getPotentialIncident(code, speed, thresholdSpeed, startTimestamp, score, cvalue, incidentsOccurredInPastInMinutes);
                return new Tuple2<Incident, InrixData>(potentialIncident, inrixDataObj);
            }
        };
    }

    private static Function<Tuple2<Incident, InrixData>, Boolean> filterPotentialIncidents() {
        return new Function<Tuple2<Incident, InrixData>, Boolean>() {
            @Override
            public Boolean call(Tuple2<Incident, InrixData> potentialIncident) throws Exception {
                String code = potentialIncident._2.getCode();
                String incident = potentialIncident._1.getIncident();
                String startTimestamp = potentialIncident._1.getStartTS();

                String eventId = potentialIncident._1.getEventId();
                if (incident.equals("INCIDENT")) {
                    return true;
                } else if (incident.equals("OVER")) {
                    String endOfIncidentTimeStamp = potentialIncident._2.getTimestamp();
                    codeToEventId.remove(code);
                    boolean updatedSuccessfully = MongoUpdate.update(inrixDataCollection, code, endOfIncidentTimeStamp, incident);
                    if (updatedSuccessfully) {

                        System.out.println("Incident " + code + " is over : " + endOfIncidentTimeStamp);
                    } else {
                        System.out.println("INCIDENT IS OVER BUT NOT UPDATED!!!!!! " + code);
                        codeToIncident.put(code, new Incident(startTimestamp, "IN_PROGRESS", 1, 4, 0, code, eventId, null, false ));
                    }
                }
                return false;
            }
        };
    }

    private static InrixData csvToInrixData(String csvRecord) throws IOException {
        String splitBy = ",";
        String[] inrixDataArray = csvRecord.split(splitBy);
        if (inrixDataArray.length == 9) {
            return new InrixData(inrixDataArray[0], inrixDataArray[1], inrixDataArray[2],
                    inrixDataArray[3], inrixDataArray[4], inrixDataArray[5], inrixDataArray[6],
                    inrixDataArray[7], inrixDataArray[8]);
        }
        return null;

    }

    private static double getThresholdSpeed(String startTimestamp, String code, Jedis jedis) {
        double thresholdSpeed;
        LocalDateTime datetime = getDateTime(startTimestamp);
        if (datetime == null) return 0.0;
        //in java Monday = 1 and Sunday = 7. Do mod 7 to match with the
        //datafile inrix_param_may31.csv where sunday=0 and saturday=6
        int weekday = 0;
        int hour = 0;
        int min = 0;
        if (startTimestamp != null) {
            weekday = datetime.getDayOfWeek().getValue() % 7;
            hour = datetime.getHour();
            min = datetime.getMinute();
        }
        double period = getPeriod(min);
        String key = Utils.genKey(code, weekday, hour);
        String tspeed = jedis.hget(key, String.valueOf(period));
        if (tspeed != null )
            try {
                thresholdSpeed = Double.parseDouble(tspeed);
            } catch (NumberFormatException e) {
                thresholdSpeed = 0.0;
//                System.out.println("Failed to parse the threshold " + code);
            }
        else
            thresholdSpeed = 0.0;    //This is an workaround. Need to correct later
        return thresholdSpeed;
    }

    private static double getPeriod(int min) {
        double period = 0.0;

        if ((min >= 0) && (min <= 14))
            period = 0.0;
        else if ((min >= 15) && (min <= 29))
            period = 1.0;
        else if ((min >= 30) && (min <= 44))
            period = 2.0;
        else if ((min >= 45) && (min <= 59))
            period = 3.0;

        return period;
    }

    private static LocalDateTime getDateTime(String startTimestamp) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss z");
        LocalDateTime datetime = null;
        try {
            datetime = LocalDateTime.parse(startTimestamp, formatter);
        } catch (Exception e) {
            System.out.println("cannot parse date");
        }
        return datetime;
    }

/*    private static Function<Tuple2<Incident, InrixData>, Document> convertToMongoDocument2() {
        return new Function<Tuple2<Incident, InrixData>, Document>() {
            @Override
            public Document call(Tuple2<Incident, InrixData> tuple2) throws Exception {
                InrixData inrixData = tuple2._2;
                String code = tuple2._2.getCode();
                LocationData locationData = JedisUtils.retrieveLocationDataFromJedis(jedis, code);
                Incident incidentDetails = tuple2._1;
                final String finalInrixString = incidentDetails.getEventId() + "," + code + "," + incidentDetails.getStartTS() + "," + null + "," + "0.0"
                        + "," + inrixData.getSpeed() + "," + inrixData.getAverage() + "," + "Inrix" + "," + Boolean.parseBoolean("false") + "," + incidentDetails.getIncidentCount() + "," + incidentDetails.getCurrentIncident() + ","
                        + locationData.getFid() + "," + locationData.getOid_1() + "," + locationData.getPrevious_code() + "," + locationData.getNext_code() + "," + locationData.getFrc() + "," + locationData.getRoadnumber() + "," + locationData.getRoadname() + ","
                        + locationData.getLinearid() + "," + locationData.getCountry() + "," + locationData.getState() + "," + locationData.getCounty() + "," + locationData.getDistrict() + "," + locationData.getMiles() + "," + locationData.getSliproad() + "," +
                        locationData.getSpecialroad() + "," + locationData.getRoadlist() + "," + locationData.getStartlat() + "," + locationData.getStartlong() + "," + locationData.getEndlat() + "," + locationData.getEndlong() + "," +
                        locationData.getBearing() + "," + locationData.getXdgroup() + "," + locationData.getShapesrid();
                String segmentLocationDataCsvToJson = CsvToJsonConvertor.segmentLocationDataCsvToJson(finalInrixString);

                Document doc = Document.parse(segmentLocationDataCsvToJson);
                return doc;
            }
        };
    }*/

    private static PairFunction<Tuple2<Incident,InrixData>, Incident, Document> convertToMongoDoc() {
        return new PairFunction<Tuple2<Incident,InrixData>, Incident, Document>() {
            @Override
            public Tuple2<Incident, Document> call(Tuple2<Incident, InrixData> incidentInrixDataTuple2) throws Exception {
                InrixData inrixData = incidentInrixDataTuple2._2;
                String code = incidentInrixDataTuple2._2.getCode();
                LocationData locationData = JedisUtils.retrieveLocationDataFromJedis(jedis, code);
                Incident incidentDetails = incidentInrixDataTuple2._1;
                final String finalInrixString = incidentDetails.getEventId() + "," + code + "," + incidentDetails.getStartTS() + "," + null + "," + "0.0"
                        + "," + inrixData.getSpeed() + "," + inrixData.getAverage() + "," + "Inrix" + "," + Boolean.parseBoolean("false") + "," + incidentDetails.getIncidentCount() + "," + incidentDetails.getCurrentIncident() + ","
                        + locationData.getFid() + "," + locationData.getOid_1() + "," + locationData.getPrevious_code() + "," + locationData.getNext_code() + "," + locationData.getFrc() + "," + locationData.getRoadnumber() + "," + locationData.getRoadname() + ","
                        + locationData.getLinearid() + "," + locationData.getCountry() + "," + locationData.getState() + "," + locationData.getCounty() + "," + locationData.getDistrict() + "," + locationData.getMiles() + "," + locationData.getSliproad() + "," +
                        locationData.getSpecialroad() + "," + locationData.getRoadlist() + "," + locationData.getStartlat() + "," + locationData.getStartlong() + "," + locationData.getEndlat() + "," + locationData.getEndlong() + "," +
                        locationData.getBearing() + "," + locationData.getXdgroup() + "," + locationData.getShapesrid() + "," + incidentDetails.isDuplicate();
                String segmentLocationDataCsvToJson = CsvToJsonConvertor.segmentLocationDataCsvToJson(finalInrixString);
                if (segmentLocationDataCsvToJson == null) return null;
                Document doc = Document.parse(segmentLocationDataCsvToJson);
                return new Tuple2<>(incidentInrixDataTuple2._1, doc);

            }
        };
    }

    //incident States: IGNORE, POTENTIAL, INCIDENT, IN_PROGRESS, MAY_BE_OVER, OVER
    private static Incident getPotentialIncident(String code, double speed, double thresholdSpeed, String startTimestamp,
                                                 int score, int cvalue, long incidentsOccurredInPastInMinutes) {
        Incident incident = codeToIncident.get(code);
        if (speed < thresholdSpeed && speed < 45.0 && (score == 30) && (cvalue >= 30)) {
            incident = updateIncidentCount(incident, code, startTimestamp, incidentsOccurredInPastInMinutes);
        } else if (codeToIncident.containsKey(code) && !incident.getIncident().equals("IGNORE")) {
            incident = updateIncidentOverCount(incident, code, incidentsOccurredInPastInMinutes);
        } else {
            incident = new Incident(startTimestamp, "IGNORE",
                    0, 0, 0, code, "Not an Event", null, false);
        }
        codeToIncident.put(code, incident);
        return incident;
    }

    private static Incident updateIncidentCount(Incident incident, String code, String startTimestamp, long incidentsOccurredInPastInMinutes) {
        if (codeToIncident.containsKey(code)) {
            int incidentCount = incident.getIncidentCount();
            String incidentState = incident.getIncident();
            incidentCount++;
            if (incidentCount < 3) {
                incident.setIncidentCount(incidentCount);
                incident.setIncident("POTENTIAL");
                incident.setOverCount(0);
                incident.setCurrentIncident(0);
                incident.setEventId("Not an event");
                System.out.println("POTENTIAL : " + incident);
            } else if (incidentCount == 3 && incidentState.equals("POTENTIAL")) {
                incident.setStartTS(startTimestamp);
                incident.setIncidentCount(incidentCount);
                incident.setOverCount(0);
                incident.setCurrentIncident(1);
                incident.setIncident("INCIDENT");
                Pair<String, Boolean> eventIDDuplicate = generateEventId("INCIDENT", code, incidentsOccurredInPastInMinutes, incident);
                incident.setEventId(eventIDDuplicate.getKey());
                incident.setDuplicate(eventIDDuplicate.getValue());
                System.out.println("INCIDENT : " + incident);
            } else {
                incident.setIncidentCount(incidentCount);
                incident.setOverCount(0);
                incident.setCurrentIncident(1);
                incident.setIncident("IN_PROGRESS");
                System.out.println("IN PROGRESS : " + incident);
            }
        } else {
            incident = new Incident(startTimestamp, "POTENTIAL",
                    0, 1, 0, code, "Not an Event", null, false);
            System.out.println("POTENTIAL : " + incident);
        }
        return incident;
    }

    private static Incident updateIncidentOverCount(Incident incident, String code, long incidentsOccurredInPastInMinutes) {
        int incidentCount = incident.getIncidentCount();
        String incidentState = incident.getIncident();
        int overCount = incident.getOverCount();
        if (incidentCount < 3) {
            incident.setCurrentIncident(0);
            incident.setIncident("IGNORE");
            incident.setOverCount(0);
            incident.setIncidentCount(0);
            incident.setEventId("Not an Event");
            incident.setEndTimestamp(null);
            System.out.println("IGNORE after POTENTIAL : " + incident);
        } else if (overCount < 5) {
            incident.setOverCount(overCount + 1);
            incident.setIncident("MAY_BE_OVER");
            incident.setCurrentIncident(1);
            incident.setIncidentCount(incidentCount);
            System.out.println("MAY Be OVER : " + incident);
        } else {
            incident.setOverCount(overCount);
            incident.setIncident("OVER");
            incident.setCurrentIncident(0);
            incident.setIncidentCount(0);
            Pair<String, Boolean> eventIDAndDuplicate = generateEventId("OVER", code, incidentsOccurredInPastInMinutes, incident);
            incident.setEventId(eventIDAndDuplicate.getKey());
            incident.setEndTimestamp(new Date(System.currentTimeMillis()).toString());
            System.out.println("OVER : " + incident);
        }
        return incident;
    }

    private static Pair<String, Boolean> generateEventId(String incident, String code, long incidentsOccurredInPastInMinutes, Incident incidentObj) {
        boolean duplicate = true;
        String eventId = "Not an event";
        if (incident.equals("OVER")) {
            eventId = codeToEventId.get(code);
            codeToEventId.remove(code);
        } else if (incident.equals("INCIDENT")) {
            String key = code + "L";
            long count = jedis.llen(key);
            List<String> codes = jedis.lrange(key, 0, count - 1);
            eventId = null;
            //Check if any incidents are in progress within 2 miles
//            System.out.println("List of codes within 2 miles: " + codes);
            for (String segmentcode : codes) {
                if (codeToEventId.get(segmentcode) != null) {
                    eventId = codeToEventId.get(segmentcode);
                    codeToEventId.put(code, eventId);
                    break;
                }
            }
            //Check if any incidents had occured within 2 miles and within last few minutes
            if (eventId == null) {
                Map<String, String> oldIncidents = loadOldIncidents(inrixDataCollection, incidentsOccurredInPastInMinutes);
//                System.out.println("Old Incidents - " + oldIncidents);
                List<String> list = codes.stream().filter(new Predicate<String>() {
                    @Override
                    public boolean test(String s) {
                        return oldIncidents.containsKey(s);
                    }
                }).collect(Collectors.toList());
                if (!list.isEmpty()) {
                    eventId = oldIncidents.get(list.get(0));
                }

            }

            if (eventId == null) {
                duplicate = false;
                System.out.println("New  event id for code " + code + " Duplicate " + duplicate);
                id++;
                Date today = new Date();
                DateFormat df2 = new SimpleDateFormat("dd-MM-yyyy");
                String dateString = df2.format(today);
                eventId = dateString + "_" + System.currentTimeMillis() + "_" + code;
                codeToEventId.put(code, eventId);
            } else {
                System.out.println("Found duplicate event id for code " + code + " Duplicate " + duplicate);
            }
        }
        return new Pair<>(eventId, duplicate);
    }

    private static Map<String, String> loadOldIncidents(MongoCollection<Document> inrixDataCollection, long minutes) {
        Map<String, String> oldCodeToEventID = new HashMap<>();
        Date startDate = new Date(System.currentTimeMillis() - TimeUnit.MINUTES.toMillis(minutes));

//        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss z");
//        LocalDateTime startDateTime = getDateTime(df.format(startDate));
        for (Document doc : MongoUpdate.getOldIncidents(inrixDataCollection, startDate)) {
//            System.out.println(doc);
//            String timestamp = String.valueOf(doc.get("end_timestamp"));
//            LocalDateTime dateTime = getDateTime(timestamp);
//            if (dateTime != null && dateTime.isAfter(startDateTime)){
                oldCodeToEventID.put(String.valueOf(doc.get("code")), String.valueOf(doc.get("event_id")));
//            }

        }
        return oldCodeToEventID;
    }

    private static void saveIncidentsToMongo(JavaDStream<Document> incidents, WriteConfig wc_incident) {
        incidents.foreachRDD(new VoidFunction<JavaRDD<Document>>() {
            @Override
            public void call(JavaRDD<Document> documentJavaRDD) throws Exception {
//                System.out.println("Saving incidents "+ documentJavaRDD.count());

                MongoSpark.save(documentJavaRDD, wc_incident);
            }
        });

    }

    private static void saveIncidentsToMongo2(JavaPairDStream<Incident, Document> incidents, WriteConfig wc_incident) {
        List<String> removeCodes = new ArrayList<>();
        incidents.foreachRDD(new VoidFunction<JavaPairRDD<Incident, Document>>() {
            @Override
            public void call(JavaPairRDD<Incident, Document> incidentsRDD) throws Exception {

                incidentsRDD.foreach(new VoidFunction<Tuple2<Incident, Document>>() {
                    @Override
                    public void call(Tuple2<Incident, Document> incidentDocumentTuple2) throws Exception {
                        boolean duplicate = false;
                        String originalCode = "";
                        String code =incidentDocumentTuple2._1.getCode();
                        String incidentStr = incidentDocumentTuple2._1.getIncident();
                        if (incidentStr.equals("INCIDENT")) {
                            String key = code + "L";
                            long count = jedis.llen(key);
                            List<String> codes = jedis.lrange(key, 0, count - 1);
                            String originalEventId = codeToEventId.get(code);
                            for (String segmentcode : codes) {
                                if (codeToEventId.containsKey(segmentcode)) {
                                    String eventId = codeToEventId.get(segmentcode);
                                    if (!eventId.equals(originalEventId))
                                        continue;
                                    removeCodes.add(code);
                                    duplicate = true;
                                    codeToEventId.put(code, eventId);
//                                    codeToIncident.remove(code);
                                    System.out.println("Removed duplicate event which is within 2 miles " + code);
                                    System.out.println("Near by code is " + segmentcode);
                                    break;
                                }
                            }

                        }
                        boolean dup = incidentDocumentTuple2._2.getBoolean("duplicate");
//                        if (!duplicate) {
                            MongoUpdate.saveIncident(inrixDataCollection, incidentDocumentTuple2._2);
                            MongoUpdate.updateStartTimeStamp(inrixDataCollection, incidentDocumentTuple2._2);
//                        }
                        if (duplicate || dup) {
                            System.out.println("Upadated duplicate event id for code " + incidentDocumentTuple2._2.getString("code"));
                            MongoUpdate.updateDuplicate(inrixDataCollection, incidentDocumentTuple2._2);
//                            MongoUpdate.updateDuplicateCode(inrixDataCollection, incidentDocumentTuple2._2, originalCode, code);
                        } else {
                            System.out.println("Original event id for code " + incidentDocumentTuple2._2.getString("code"));

                        }
                    }
                });
            }
        });


//        incidents.foreachRDD(new VoidFunction<JavaRDD<Document>>() {
//            @Override
//            public void call(JavaRDD<Document> documentJavaRDD) throws Exception {
////                System.out.println("Saving incidents "+ documentJavaRDD.count());
//
//                MongoSpark.save(documentJavaRDD, wc_incident);
//            }
//        });

    }
}
