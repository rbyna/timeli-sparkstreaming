package com.timeli.spark.streams;

import com.mongodb.spark.config.WriteConcernConfig;
import com.mongodb.spark.config.WriteConfig;
import com.timeli.spark.misc.ExternalConfig;
import com.timeli.spark.utils.JavaCustomInrixReceiver;
import org.apache.log4j.Logger;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.streaming.Durations;
import org.apache.spark.streaming.api.java.JavaDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.*;

import static com.timeli.spark.misc.Constants.*;

public class ReadingStreams {

    private static Logger log;

    private static JavaStreamingContext jssc;


    public static void main(String[] args) throws Exception {

        ExternalConfig configProperties = getConfigProperties();

        initialize(configProperties);

        executeInrixStreams(configProperties);

        startStreamingContext();

    }

    private static void startStreamingContext() throws InterruptedException {
        // Start the computation*/
        //================================================================================================================
        jssc.start();
        jssc.awaitTermination();
    }

    private static void executeInrixStreams(ExternalConfig configProperties) throws Exception {
        System.out.println(configProperties.toString());
        JavaDStream<String> inrixData = jssc.receiverStream(new JavaCustomInrixReceiver(configProperties.getInrixDirPath(),
                configProperties.getInrixFilename()));
        InrixStreams.initialize(configProperties);
        InrixStreams.processInrixStreams(inrixData, configProperties);

    }

    private static void initialize(ExternalConfig config) throws Exception {

        log = SparkStreamConfiguration.setLogLevels();
        JavaSparkContext jsc = SparkStreamConfiguration.createJavaSparkContext(config);
        jsc.setLogLevel("ERROR");

        jssc = new JavaStreamingContext(jsc, Durations.seconds(60));
    }

    private static ExternalConfig getConfigProperties() throws Exception {
        Properties mainProperties = new Properties();
        mainProperties.load(new FileInputStream("./application.properties"));
        ExternalConfig config = new ExternalConfig();
        config.setInrixDirPath(mainProperties.getProperty("archive.inrix.realtimedata.folder"));
        config.setMongoDbName(mainProperties.getProperty("mongoDbName"));
        config.setMongoCollectionNameForProcessedInrixData(mainProperties.getProperty("mongoCollectionNameForProcessedData"));
        config.setMongoCollectionNameForRawInrixData(mainProperties.getProperty("mongoCollectionNameForRawData"));
        config.setMongoConnectionString(mainProperties.getProperty("mongoConnectionString"));
        config.setIncidentsOccurredInPastInMinutes(mainProperties.getProperty("incidentsOccurredInPastInMinutes"));
        config.setInrixFilename(mainProperties.getProperty("archive.inrix.file.name"));
        return config;
    }
}
