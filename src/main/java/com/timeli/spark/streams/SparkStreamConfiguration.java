package com.timeli.spark.streams;

import com.timeli.spark.misc.ExternalConfig;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;

import static com.timeli.spark.misc.Constants.*;

public class SparkStreamConfiguration {

    public static Logger setLogLevels() {
        final Logger log;
        boolean log4jInitialized = Logger.getRootLogger().getAllAppenders().hasMoreElements();
        if (!log4jInitialized) {
            // We first log something to initialize Spark's default logging, then we override the
            // logging level.
            Logger.getLogger(ReadingStreams.class).info("Setting log level to [WARN] for streaming example." +
                    " To override add a custom log4j.properties to the classpath.");
            Logger.getRootLogger().setLevel(Level.WARN);
            log = Logger.getRootLogger();

        } else {
            log = null;
        }
        return log;
    }

    public static JavaSparkContext createJavaSparkContext(final ExternalConfig config) {
        // String uri = getMongoClientURI(args);
        //  dropDatabase(uri);

        SparkConf conf = new SparkConf()
                .setMaster("local[2]")
                .setAppName("MongoSparkConnectorTour")
                .set("spark.app.id", "MongoSparkConnectorTour")
                .set("spark.mongodb.input.uri", config.getMongoConnectionString())
                .set("spark.mongodb.output.uri", config.getMongoConnectionString())
                .set("spark.mongodb.input.database", config.getMongoDbName())

                .set("spark.mongodb.output.database", config.getMongoDbName())
                .set("spark.mongodb.input.collection", config.getMongoCollectionNameForProcessedInrixData())
                .set("spark.mongodb.output.collection", config.getMongoCollectionNameForProcessedInrixData())
                .set("spark.network.timeout", "600s");
        //     .set("spark.streaming.kafka.consumer.poll.ms", "4096");

        return new JavaSparkContext(conf);
    }


}
