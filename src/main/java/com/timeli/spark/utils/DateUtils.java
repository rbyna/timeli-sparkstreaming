package com.timeli.spark.utils;

import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtils {

    public static Date convertTime(String timestamp) {
        if (timestamp == null) return null;
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss z");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");

        Date dateString = df.parse(timestamp, new ParsePosition(0));//2018-10-13 18:41:07 UTC
        if(dateString == null) return null;;
        String newDateStr = sdf.format(dateString);
//        System.out.println(newDateStr);
        Date newDate = sdf.parse(newDateStr, new ParsePosition(0));
//        System.out.println(newDate);
        return newDate;
    }
}
