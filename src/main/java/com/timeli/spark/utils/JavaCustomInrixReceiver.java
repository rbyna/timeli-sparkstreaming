package com.timeli.spark.utils;


import java.io.*;
import java.net.ConnectException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.util.function.Consumer;
import java.util.stream.Stream;

import org.apache.commons.io.FileUtils;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.spark.storage.StorageLevel;
import org.apache.spark.streaming.receiver.Receiver;

public class JavaCustomInrixReceiver extends Receiver<String>{
//    String fileName = null;
    
    public static final String PATTERN_MM_DD_YYYY = "MM-dd-yyyy";

    public static String _dir;

    private static String file;

    public JavaCustomInrixReceiver(String _dir, String file){
    	

        super(StorageLevel.MEMORY_AND_DISK_2());
        this._dir = _dir;
        this.file =file;
        
        //System.out.println("file location: " + _dir);
        

    }

    @Override
    public void onStart(){
        new Thread(this::receive).start();
    }

    @Override
    public void onStop() {
        // There is nothing much to do as the thread calling receive()
        // is designed to stop by itself if isStopped() returns false
    }

    /** Create a FileStreamResder and keep on receiving data*/
    private void receive() {
        String fileName = null;
        SimpleDateFormat sdf = new SimpleDateFormat(PATTERN_MM_DD_YYYY);
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));

        fileName = _dir + "/" + file;
        System.out.println("Filename to be retrieved : " + fileName);

        //Socket socket = null;
        String userInput = null;

        try {
            // connect to the server
            //socket = new Socket(host, port);

        	File infile = null;
        	//It is an workaround, should be removed later when file update will be fixed without deletion
        	while(infile == null) {
        		try {
        			infile = new File(fileName);
        		}catch(Exception e) {
        			System.out.println("No such file exists");
        		}
        	}

         	Stream<String> stream = Files.readAllLines(Paths.get(fileName)).stream();

            try {

                stream.forEach(new Consumer<String>() {
                    @Override
                    public void accept(String inrixstring) {
                        long time = System.currentTimeMillis();
                        //System.out.println(timestamp);
                        //String s1 = timestamp + ',' + s;
                        if(inrixstring.contains("code"))
                        	return;
                        store(inrixstring);
                        //producer.send(new ProducerRecord<String, String>("test", s1));
                    }
                });
            }finally {

                System.out.println("Start time : " + new Date(System.currentTimeMillis()));
                stream.close();
            }

        	while(true) {
        		File tempFile = null;
        		//It is an workaround, should be removed later when file update will be fixed without deletion
        		while(tempFile == null) {
        			try {
        				tempFile = new File(fileName);
        			} catch(Exception e) {
        				System.out.println("No such file exists");
        			}
        		}

        		boolean isTwoEqual = FileUtils.contentEquals(infile, tempFile);

        		if(isTwoEqual)
        			continue;

        		infile = tempFile;

        		Stream<String> stream2 = null;


                while(stream2 == null) {
                    try {
                        stream2 = Files.readAllLines(Paths.get(fileName)).stream();
                    } catch(Exception e) {

                    }
                }

                System.out.println("Stream count : " + stream2.count());

                try {

                    stream2.forEach(new Consumer<String>() {
                        @Override
                        public void accept(String inrixDataString) {
                            long time = System.currentTimeMillis();
                            //System.out.println(timestamp);
                            //String s1 = timestamp + ',' + s;

                            if(inrixDataString.contains("code"))
                                return;
                            store(inrixDataString);
                            //producer.send(new ProducerRecord<String, String>("test", s1));
                        }
                    });
                } finally {

                    System.out.print("Start time : " + new Date(System.currentTimeMillis()));
                    stream2.close();
                }
                Thread.sleep(3000);

        	}

      } catch(ConnectException ce) {
            // restart if could not connect to server
            restart("Could not connect", ce);
        } catch(Throwable t) {
            // restart if there is any other error
            restart("Error receiving data", t);
        }
    }

    private boolean isCompletelyWritten(File file) {
        RandomAccessFile stream = null;
        try {
            stream = new RandomAccessFile(file, "rw");
            return true;
        } catch (Exception e) {
            System.out.println("Skipping file " + file.getName() + " for this iteration due it's not completely written");
        } finally {
            if (stream != null) {
                try {
                    stream.close();
                } catch (IOException e) {
                    System.out.println("Exception during closing file " + file.getName());
                }
            }
        }
        return false;
    }
}