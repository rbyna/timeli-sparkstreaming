package com.timeli.spark.utils;

import com.timeli.spark.obj.LocationData;
import redis.clients.jedis.Jedis;

public class JedisUtils {

    public static LocationData retrieveLocationDataFromJedis(Jedis jedis, String code) {

        return new LocationData(code,  jedis.hget(code, "previous_code"), jedis.hget(code, "next_code"),
                jedis.hget(code, "frc"), jedis.hget(code, "fid"), jedis.hget(code, "oid_1"),
                jedis.hget(code, "linearid"), jedis.hget(code, "road"), jedis.hget(code, "road_number"),
                jedis.hget(code, "country"), jedis.hget(code, "state"), jedis.hget(code, "district"),
                jedis.hget(code, "county"), jedis.hget(code, "distance"), jedis.hget(code, "sliproad"),
                jedis.hget(code, "specialroad"), jedis.hget(code, "roadlist"), jedis.hget(code, "startlat"),
                jedis.hget(code, "startlong"), jedis.hget(code, "endlat"), jedis.hget(code, "endlong"),
                jedis.hget(code, "bearing"), jedis.hget(code, "xdgroup"), jedis.hget(code, "shapesrid"));
    }
}
