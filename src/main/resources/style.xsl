<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="text" omit-xml-declaration="yes" indent="no"/>
    <xsl:template match="/">
        timestamp, reference, average, score, code, c-value, type, travelTimeMinutes, speed
        <xsl:for-each select="//SegmentSpeedResults">
            <xsl:value-of select="@timestamp" />
            <xsl:for-each select="//Segment">
                <xsl:value-of select="@reference" />
                <xsl:value-of select="concat(',')"/>
                <xsl:value-of select="@average" />
                <xsl:value-of select="concat(',')"/>
                <xsl:value-of select="@score" />
                <xsl:value-of select="concat(',')"/>
                <xsl:value-of select="@code" />
                <xsl:value-of select="concat(',')"/>
                <xsl:value-of select="@c-value" />
                <xsl:value-of select="concat(',')"/>
                <xsl:value-of select="@type" />
                <xsl:value-of select="concat(',')"/>
                <xsl:value-of select="@travelTimeMinutes" />
                <xsl:value-of select="concat(',')"/>
                <xsl:value-of select="@speed" />
            </xsl:for-each>
        </xsl:for-each>
    </xsl:template>
</xsl:stylesheet>